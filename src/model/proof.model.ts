import { Context } from '../jsonld/jsonld.types'

export const JWS2020_CONTEXT = 'https://w3id.org/security/suites/jws-2020/v1'

export interface Proof {
  '@context'?: Context
  type: 'JsonWebSignature2020'
  created: string
  jws?: string
  proofPurpose: string
  verificationMethod: string
}
