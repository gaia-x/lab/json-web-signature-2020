import {documentLoaders} from 'jsonld'

import CredentialExamplesContext from '../context/credentials_examples_v1_context.json'
import CredentialContext from '../context/credentials_v1_context.json'
import JwsContext from '../context/jws2020_v1_context.json'
import OdrlContext from '../context/odrl_context.json'
import {DocumentLoader, RemoteDocument} from './jsonld.types'

/**
 * Loads contexts from the filesystem or from a cache instead of resolving them through the network all the time.
 * <p>
 *   This class is very useful in the case of rate limiting due to a high number of calls or for running tests in a
 *   controllable environment.
 * </p>
 * <p>
 *   To use your own contexts, you can call the {@link addContext} method.
 *   To mark a context to be cached, you can call the {@link addCachedContext} method.
 *
 *   ```typescript
 *   import MyContext from 'contexts/my-context.json'
 *   import CustomContext from 'contexts/custom-context.json'
 *
 *   const offlineDocumentLoader: DocumentLoader = new OfflineDocumentLoaderBuilder()
 *     .addContext('https://my-context.org/2024/entities/v1', MyContext)
 *     .addContext('https://custom-context.org/2024/entities/v1', CustomContext)
 *     .addCachedContext('https://my-context-to-cache')
 *     .build()
 *   ```
 */
export class OfflineDocumentLoaderBuilder {
    private readonly CACHING_TIME_IN_SECONDS = 60 * 10 * 1000 // 600s = 10min
    private readonly contexts: Map<string, any> = new Map<string, any>([
        ['https://www.w3.org/2018/credentials/v1', CredentialContext],
        ['https://www.w3.org/2018/credentials/examples/v1', CredentialExamplesContext],
        ['https://www.w3.org/ns/odrl.jsonld', OdrlContext],
        ['https://w3id.org/security/suites/jws-2020/v1', JwsContext]
    ])

    private readonly cachedContextURIs: string[] = []
    private readonly cacheExpiry: Map<string, Date> = new Map<string, Date>()
    private readonly cachedContexts: Map<string, any> = new Map<string, any>()

    addContext(url: string, context: any) {
        this.contexts.set(url, context)
        return this
    }

    addCachedContext(url: string) {
        this.cachedContextURIs.push(url)
        return this
    }

    build(): DocumentLoader {
        const nodeDocumentLoader: DocumentLoader = documentLoaders.node()

        return async (url: string): Promise<RemoteDocument> => {
            if (this.contexts.has(url)) {
                return {
                    document: this.contexts.get(url),
                    documentUrl: url
                }
            } else if (this.cachedContextURIs.indexOf(url) > -1) {
                return this.loadFromCache(url)
            }

            return nodeDocumentLoader(url)
        }
    }

    async loadFromCache(url: string): Promise<RemoteDocument> {
        const cacheEntry = this.cacheExpiry.get(url)
        if (cacheEntry && cacheEntry > new Date()) {
            return {
                document: this.cachedContexts.get(url),
                documentUrl: url
            }
        } else {
            try {
                const nodeDocumentLoader: DocumentLoader = documentLoaders.node()
                const remoteDocument = await nodeDocumentLoader(url)
                this.cachedContexts.set(url, remoteDocument.document)
                this.cacheExpiry.set(url, new Date(new Date().getTime() + this.CACHING_TIME_IN_SECONDS))
                return remoteDocument
            } catch (error) {
                console.error("An error occurred while retrieving a remote context", error);
            }
            throw new Error(
                `URL "${url}" could not be dereferenced`)
        }
    }
}
