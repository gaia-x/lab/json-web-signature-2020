import { ArrayContext, Context } from './jsonld.types'

/**
 * This specializes in working with JSON-LD contexts to add URIs and more to come.
 *
 * Please note that aliased contexts (i.e. <code>"@context": { "example": "https://example.org/schema/" }</code>) are
 * not supported at the moment due to complexities during JSON-LD expansion/compaction.
 */
export class ContextHandler {
  private context: Set<string>

  private constructor(context: ArrayContext) {
    this.context = new Set<string>(context)
  }

  static fromContext(context: Context): ContextHandler {
    if (!Array.isArray(context)) {
      throw new Error('Aliased contexts are not supported')
    }

    return new ContextHandler(structuredClone(context) as ArrayContext)
  }

  add(uri: string): ContextHandler {
    this.context.add(uri)

    return this
  }

  toContext(): ArrayContext {
    return Array.from(this.context.values())
  }
}
