import { describe, expect, it } from 'vitest'

import { ContextHandler } from './context.handler'
import { AliasedContext, ArrayContext } from './jsonld.types'

describe('ContextHandler', () => {
  it('should load an array context and return it', () => {
    const context: ArrayContext = ['https://www.w3.org/2018/credentials/v1', 'https://www.w3.org/2018/credentials/examples/v1']
    const contextHandler: ContextHandler = ContextHandler.fromContext(context)

    expect(contextHandler.toContext()).toEqual(context)
  })

  it('should throw an error when loading an aliased context', () => {
    const context: AliasedContext = {
      cred: 'https://www.w3.org/2018/credentials/v1',
      ex: 'https://www.w3.org/2018/credentials/examples/v1'
    }

    expect(() => ContextHandler.fromContext(context)).toThrow('Aliased contexts are not supported')
  })

  it('should add URIs to a context', () => {
    const context: ArrayContext = ['https://www.w3.org/2018/credentials/v1', 'https://www.w3.org/2018/credentials/examples/v1']
    const contextHandler: ContextHandler = ContextHandler.fromContext(context)
      .add('https://www.w3.org/ns/odrl.jsonld')
      .add('https://w3id.org/security/suites/jws-2020/v1')

    expect(contextHandler.toContext()).toEqual([
      'https://www.w3.org/2018/credentials/v1',
      'https://www.w3.org/2018/credentials/examples/v1',
      'https://www.w3.org/ns/odrl.jsonld',
      'https://w3id.org/security/suites/jws-2020/v1'
    ])
  })

  it('should not add URI to a context if it is already present', () => {
    const context: ArrayContext = ['https://www.w3.org/2018/credentials/v1', 'https://www.w3.org/2018/credentials/examples/v1']
    const contextHandler: ContextHandler = ContextHandler.fromContext(context)
      .add('https://www.w3.org/2018/credentials/v1')
      .add('https://w3id.org/security/suites/jws-2020/v1')

    expect(contextHandler.toContext()).toEqual([
      'https://www.w3.org/2018/credentials/v1',
      'https://www.w3.org/2018/credentials/examples/v1',
      'https://w3id.org/security/suites/jws-2020/v1'
    ])
  })
})
