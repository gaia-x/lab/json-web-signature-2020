export declare interface RemoteDocument {
  contextUrl?: string
  document: string
  documentUrl: string
}

export declare type DocumentLoader = (url: string) => Promise<RemoteDocument>

export declare type ArrayContext = string[]
export declare type AliasedContext = { [alias: string]: string }
export declare type Context = ArrayContext | AliasedContext
