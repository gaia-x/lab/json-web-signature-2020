import { DidResolver } from '../resolver/did.resolver'
import { Options } from './options'

export interface VerifierOptions extends Options {
  /**
   * Override the {@link DidResolver} used to resolve {@link DIDDocument}s for proof verification
   */
  didResolver?: DidResolver
}
