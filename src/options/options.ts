import { DocumentLoader } from '../jsonld/jsonld.types'

export interface Options {
  /**
   * Override the {@code jsonld} library's document loader
   */
  documentLoader?: DocumentLoader

  /**
   * Use the {@code jsonld} library's safe mode during document processing
   */
  safe?: boolean
}
