export class DidResolutionException extends Error {
  constructor(did: string, cause: string) {
    super(`${did} couldn't be resolved as a valid DID, resolution failed with the following cause : ${cause}`)
  }
}
