import JsonLdError from 'jsonld/lib/JsonLdError'
import { describe, expect, it } from 'vitest'

import { DocumentLoader } from '../jsonld/jsonld.types'
import { OfflineDocumentLoaderBuilder } from '../jsonld/offline-document.loader'
import { VerifiableCredential } from '../model/verifiable-credential.model'
import { GaiaXSignatureHasher } from './gaia-x-signature.hasher'

describe('GaiaXSignatureHasher', () => {
  const verifiableCredential: Omit<VerifiableCredential, 'proof'> = {
    '@context': ['https://www.w3.org/2018/credentials/v1', 'https://www.w3.org/2018/credentials/examples/v1'],
    id: 'http://example.edu/credentials/1872',
    type: ['VerifiableCredential', 'AlumniCredential'],
    issuer: 'https://example.edu/issuers/565049',
    issuanceDate: '2010-01-01T19:23:24Z',
    credentialSubject: {
      id: 'did:example:ebfeb1f712ebc6f1c276e12ec21',
      alumniOf: {
        id: 'did:example:c276e12ec21ebfeb1f712ebc6f1'
      },
      name: 'John Doe'
    }
  }

  const offlineDocumentLoader: DocumentLoader = new OfflineDocumentLoaderBuilder().build()
  const hasher: GaiaXSignatureHasher = new GaiaXSignatureHasher({ documentLoader: offlineDocumentLoader })

  it('should canonize the document only and create a SHA-256 hash of it', async () => {
    expect(await hasher.hash(verifiableCredential)).toEqual(
      new TextEncoder().encode('e76b58f264e8ef301b853c3527534e167dd0e2b70d0c7fdb049cdd77a6fce5e1')
    )
  })

  it('should throw an exception if document is invalid JSON-LD', async () => {
    const invalidJsonLd = structuredClone(verifiableCredential)
    invalidJsonLd['@context'] = []

    try {
      await hasher.hash(invalidJsonLd)
    } catch (e) {
      expect(e).toBeInstanceOf(JsonLdError)

      return
    }

    throw new Error()
  })
})
