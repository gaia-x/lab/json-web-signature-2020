import JsonLdError from 'jsonld/lib/JsonLdError'
import { describe, expect, it } from 'vitest'

import { DocumentLoader } from '../jsonld/jsonld.types'
import { OfflineDocumentLoaderBuilder } from '../jsonld/offline-document.loader'
import { Proof } from '../model/proof.model'
import { VerifiableCredential } from '../model/verifiable-credential.model'
import { JsonWebSignature2020Hasher } from './json-web-signature-2020.hasher'

describe('JsonWebSignature2020Hasher', () => {
  const document: Omit<VerifiableCredential, 'proof'> = {
    '@context': ['https://www.w3.org/2018/credentials/v1', 'https://www.w3.org/2018/credentials/examples/v1'],
    id: 'http://example.edu/credentials/1872',
    type: ['VerifiableCredential', 'AlumniCredential'],
    issuer: 'https://example.edu/issuers/565049',
    issuanceDate: '2010-01-01T19:23:24Z',
    credentialSubject: {
      id: 'did:example:ebfeb1f712ebc6f1c276e12ec21',
      alumniOf: {
        id: 'did:example:c276e12ec21ebfeb1f712ebc6f1'
      },
      name: 'John Doe'
    }
  }

  const proof: Proof = {
    type: 'JsonWebSignature2020',
    created: '2024-01-31T16:00:45.490Z',
    proofPurpose: 'assertionMethod',
    verificationMethod:
      'did:web:wizard.lab.gaia-x.eu:api:credentials:2d37wbGvQzbAQ84yRouh2m2vBKkN8s5AfH9Q75HZRCUQmJW7yAVSNKzjJj6gcjE2mDNDUHCichXWdMH3S2c8AaDLm3kXmf5R8DLGYw32T5A1uaxbEy5W28Tv5BDbSrdfGtCUpjC8RHpZAMFTDET3g3QkFTRuY8rVrR7zjeSWa54WeRLKutKhyR5EbdyNYPWHxm8TEWWuMchWEBnXQNjrntGUEP8csESeaTBCupBqxSJ8WM4fBRwFvJKmCLwJzkpo2LkbWEzpGRy3GvedQ1AFLEe3JCdcgs5b2u5ubgT3pgte71JebuiAPP8jJN3tUAhk9CAPXpu2EHvCuy4C1CuYK5pnMKwRHtRsA2w7i1Gn7EzMrRjiU5AeFS3KLMbbHNLVvuqCeW9Gx93tUfJdQx2Z88obsrH9sWYSCDFExmJE9w31uricJmQcb9815znjuupWrGb3jy32qX7Vvt9uya7keewZuAQ1TjyctKDcqWA44JuKVURtdtykEUuKoUZHBasJ4vaBaBfmy7MBkhFsqPRVp9MdkTwVGv5mHxV6SayZRaN7WoJCWu7Jphb3uB4oEXQXsP4EShYzyqUM8yTrrFtHADiGWDw8CZ86jEvfA7n#JWK2020',
    jws: 'eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..K_H43yH3fCCzUN69uPnxmiNxeyf1xvqrJG2_xxf1r3v31VeQXHaVM38TORTA2zqZoNL4bRzHQUnGbnW9g0MPSXvRLONvmeXdCUwWJcS7xaqFgX4EZjugfPQmfwYZpuqbPICubwUAc4bXVph3GBbVDQyWMBoRw8lJIpXG_frIEhIoEq56ItAA5PtwfgdIuWKoz9Z43msjtbZpBZXVWwXoztceHVPPioLfd-fa4AlhcemF_O621QDsmmkDF2WOW5whpaC1cBEu58wN2Cw35zjNTuLsJ8eiTEiE1hfMqig_75-1i6cw8T9EOJPhVqgRQ-Ept_ZzdPpd52m3dvspyv-S9w'
  }

  const offlineDocumentLoader: DocumentLoader = new OfflineDocumentLoaderBuilder().build()
  const hasher: JsonWebSignature2020Hasher = new JsonWebSignature2020Hasher({ documentLoader: offlineDocumentLoader })

  it('should canonize the proof and document and create a merged SHA-256 hash of them', async () => {
    expect(await hasher.hash(document, proof)).toEqual(
      Buffer.from([
        55, 65, 185, 111, 65, 195, 125, 156, 53, 221, 213, 247, 53, 123, 167, 52, 9, 236, 251, 6, 24, 190, 151, 253, 230, 73, 252, 250, 46, 255, 100,
        101, 231, 107, 88, 242, 100, 232, 239, 48, 27, 133, 60, 53, 39, 83, 78, 22, 125, 208, 226, 183, 13, 12, 127, 219, 4, 156, 221, 119, 166, 252,
        229, 225
      ])
    )
    expect(proof['@context']).toBeUndefined()
  })

  it('should throw an exception if document is invalid JSON-LD', async () => {
    const invalidJsonLd = structuredClone(document)
    invalidJsonLd['@context'] = []

    try {
      await hasher.hash(invalidJsonLd, proof)
    } catch (e) {
      expect(e).toBeInstanceOf(JsonLdError)

      return
    }

    throw new Error()
  })
})
