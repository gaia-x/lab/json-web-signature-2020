import { canonize } from 'jsonld'

import { ContextHandler } from '../jsonld/context.handler'
import { ArrayContext } from '../jsonld/jsonld.types'
import { JWS2020_CONTEXT, Proof } from '../model/proof.model'
import { VerifiableCredential } from '../model/verifiable-credential.model'
import { Hasher } from './hasher'

export class JsonWebSignature2020Hasher extends Hasher {
  async hash(document: Omit<VerifiableCredential, 'proof'>, inputProof: Proof): Promise<Buffer | Uint8Array> {
    const canonizedData: string = await canonize(document, {
      algorithm: 'URDNA2015',
      format: 'application/n-quads',
      safe: this.safe,
      documentLoader: this.documentLoader
    })

    const proofContext: ArrayContext = ContextHandler.fromContext(document['@context']).add(JWS2020_CONTEXT).toContext()

    const proof: Proof = structuredClone(inputProof)
    proof['@context'] = proofContext
    delete proof.jws
    const canonizedProof: string = await canonize(proof, {
      algorithm: 'URDNA2015',
      format: 'application/n-quads',
      safe: this.safe,
      documentLoader: this.documentLoader
    })

    const hashedProof: Buffer = this.sha256(canonizedProof).digest()
    const hashedData: Buffer = this.sha256(canonizedData).digest()

    return Buffer.concat([hashedProof, hashedData])
  }
}
