import { Hash, createHash } from 'crypto'
import { documentLoaders } from 'jsonld'

import { DocumentLoader } from '../jsonld/jsonld.types'
import { Proof } from '../model/proof.model'
import { VerifiableCredential } from '../model/verifiable-credential.model'
import { Options } from '../options/options'

/**
 * Hasher classes are used to transform a verifiable credential split into its document and its proof to a normalized
 * hash that can be used to sign and verify itself.
 *
 * A {@link DocumentLoader} can be passed to the constructor in order to override the behaviour of the JSON-LD document
 * loader.
 */
export abstract class Hasher {
  protected readonly safe: boolean
  protected readonly documentLoader: DocumentLoader

  constructor(options?: Options) {
    this.safe = options?.safe ?? true
    this.documentLoader = options?.documentLoader ?? documentLoaders.node()
  }

  abstract hash(document: Omit<VerifiableCredential, 'proof'>, proof?: Proof): Promise<Buffer | Uint8Array>

  protected sha256(payload: string): Hash {
    const h = createHash('sha256')
    h.update(payload)

    return h
  }
}
