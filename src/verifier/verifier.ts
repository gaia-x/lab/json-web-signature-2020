import { DIDDocument, JsonWebKey, VerificationMethod } from 'did-resolver'
import { KeyLike, flattenedVerify, importJWK } from 'jose'
import { DateTime } from 'luxon'

import { Hasher } from '../hasher/hasher'
import { Proof } from '../model/proof.model'
import { VerifiableCredential } from '../model/verifiable-credential.model'
import { VerifierOptions } from '../options/verifier.options'
import { DidResolver } from '../resolver/did.resolver'
import { MalformedProofException } from './exception/malformed-proof.exception'
import { SignatureValidationException } from './exception/signature-validation.exception'
import { UnresolvableVerificationMethodException } from './exception/unresolvable-verification-method.exception'

/**
 * This class' responsibility is to ensure that verifiable credentials are untampered between their creation/signing
 * and their usage through this library.
 */
export abstract class Verifier {
  private readonly didResolver: DidResolver
  private readonly hasher: Hasher

  protected constructor(hasher: Hasher, options?: VerifierOptions) {
    this.hasher = hasher
    this.didResolver = options?.didResolver ?? new DidResolver()
  }

  /**
   * Verifies that the verifiable credential and its proof match the given JWS.
   *
   * @param verifiableCredential the {@link VerifiableCredential} to validate
   *
   * @throws MalformedProofException when the proof is malformed, more details are given in the exception's message
   * @throws UnresolvableVerificationMethodException when the verification method specified in the proof can't be resolved to collect the issuer's public key
   * @throws SignatureValidationException when the signature given in the proof does not match the verifiable credential's content, this usually happens when the verifiable credential has been tampered
   */
  async verify(verifiableCredential: VerifiableCredential): Promise<void> {
    // Cloning the input verifiable credential avoids modifying the calling method's object
    const { proof, document } = this.separateProofAndDocument(verifiableCredential)
    const jws: string = structuredClone(proof.jws) as string

    const publicKey = await this.extractPublicKey(proof)
    const hash: Buffer | Uint8Array = await this.hasher.hash(document, proof)

    const jwsParts: string[] = jws.split('.')

    try {
      await flattenedVerify({ protected: jwsParts[0], payload: hash, signature: jwsParts[2] }, publicKey)
    } catch (e) {
      throw new SignatureValidationException(verifiableCredential.id)
    }
  }

  private separateProofAndDocument(verifiableCredential: VerifiableCredential): { proof: Proof; document: Omit<VerifiableCredential, 'proof'> } {
    // Cloning the input verifiable credential avoids modifying the calling method's object
    const { proof, ...document }: VerifiableCredential = structuredClone(verifiableCredential)

    if (proof.type != 'JsonWebSignature2020') {
      throw new MalformedProofException(`${proof.type} proof type is not supported`)
    }

    const parsedDate = DateTime.fromISO(proof.created)
    if (proof.created && !parsedDate.isValid) {
      throw new MalformedProofException(`Please provide a valid proof 'created' attribute, ${proof.created} is invalid`)
    }

    if (!proof.jws) {
      throw new MalformedProofException(`The 'jws' attribute is missing in the proof, this isn't a valid verifiable credential`)
    }

    return { proof, document }
  }

  private async extractPublicKey(proof: Proof): Promise<KeyLike | Uint8Array> {
    const [did, verificationMethodId] = proof.verificationMethod.split('#')
    const didDocument: DIDDocument = await this.didResolver.resolve(did)

    let verificationMethod: VerificationMethod | undefined = didDocument.verificationMethod?.find(v => v.id === proof.verificationMethod)
    if (!verificationMethod) {
      verificationMethod = didDocument.publicKey?.find(p => p.id === `#${verificationMethodId}`)
    }

    if (!verificationMethod) {
      throw new UnresolvableVerificationMethodException(proof.verificationMethod)
    }

    const jwk: JsonWebKey = verificationMethod.publicKeyJwk!

    return await importJWK(jwk)
  }
}
