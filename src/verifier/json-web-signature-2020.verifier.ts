import { JsonWebSignature2020Hasher } from '../hasher/json-web-signature-2020.hasher'
import { VerifierOptions } from '../options/verifier.options'
import { Verifier } from './verifier'

/**
 *  Allows to verify verifiable credential proofs that use follow
 *  <a href="https://www.w3.org/community/reports/credentials/CG-FINAL-lds-jws2020-20220721/">the Json Web Signature 2020</a> specification
 */
export class JsonWebSignature2020Verifier extends Verifier {
  constructor(options?: VerifierOptions) {
    super(new JsonWebSignature2020Hasher(options), options)
  }
}
