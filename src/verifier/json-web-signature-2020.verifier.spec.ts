import { DIDDocument } from 'did-resolver'
import { MockedFunction, beforeEach, describe, expect, it, vi } from 'vitest'

import { DocumentLoader } from '../jsonld/jsonld.types'
import { OfflineDocumentLoaderBuilder } from '../jsonld/offline-document.loader'
import { VerifiableCredential } from '../model/verifiable-credential.model'
import { DidResolver } from '../resolver/did.resolver'
import { MalformedProofException } from './exception/malformed-proof.exception'
import { SignatureValidationException } from './exception/signature-validation.exception'
import { UnresolvableVerificationMethodException } from './exception/unresolvable-verification-method.exception'
import { JsonWebSignature2020Verifier } from './json-web-signature-2020.verifier'

vi.mock('../resolver/did.resolver')

describe('JsonWebSignature2020Verifier', () => {
  const verifiableCredential: VerifiableCredential = {
    '@context': [
      'https://www.w3.org/2018/credentials/v1',
      'https://www.w3.org/2018/credentials/examples/v1',
      'https://w3id.org/security/suites/jws-2020/v1'
    ],
    id: 'http://example.gov/credentials/3732',
    type: ['VerifiableCredential', 'UniversityDegreeCredential'],
    issuer: {
      id: 'https://example.com/issuer/123'
    },
    issuanceDate: '2020-03-10T04:24:12.164Z',
    credentialSubject: {
      id: 'did:example:456',
      degree: {
        type: 'BachelorDegree',
        name: 'Bachelor of Science and Arts'
      }
    },
    proof: {
      type: 'JsonWebSignature2020',
      created: '2019-12-11T03:50:55Z',
      jws: 'eyJhbGciOiJFZERTQSIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..MJ5GwWRMsadCyLNXU_flgJtsS32584MydBxBuygps_cM0sbU3abTEOMyUvmLNcKOwOBE1MfDoB1_YY425W3sAg',
      proofPurpose: 'assertionMethod',
      verificationMethod: 'https://example.com/issuer/123#ovsDKYBjFemIy8DVhc-w2LSi8CvXMw2AYDzHj04yxkc'
    }
  }

  const didResolver: DidResolver = new DidResolver()
  const offlineDocumentLoader: DocumentLoader = new OfflineDocumentLoaderBuilder().build()
  const verifier: JsonWebSignature2020Verifier = new JsonWebSignature2020Verifier({ didResolver, documentLoader: offlineDocumentLoader })

  beforeEach(() => {
    ;(didResolver.resolve as MockedFunction<typeof didResolver.resolve>).mockClear()
    vi.spyOn(didResolver, 'resolve').mockImplementation((did: string): Promise<DIDDocument> => {
      return Promise.resolve({
        '@context': ['https://www.w3.org/ns/did/v1', 'https://w3id.org/security/suites/jws-2020/v1'],
        id: 'https://example.com/issuer/123',
        publicKey: [
          {
            id: '#ovsDKYBjFemIy8DVhc-w2LSi8CvXMw2AYDzHj04yxkc',
            type: 'JsonWebKey2020',
            controller: 'https://example.com/issuer/123',
            publicKeyJwk: {
              kty: 'OKP',
              crv: 'Ed25519',
              x: 'CV-aGlld3nVdgnhoZK0D36Wk-9aIMlZjZOK2XhPMnkQ'
            }
          }
        ],
        assertionMethod: ['#ovsDKYBjFemIy8DVhc-w2LSi8CvXMw2AYDzHj04yxkc']
      })
    })
  })

  it('should validate JsonWebSignature2020 specification test vectors', async () => {
    await verifier.verify(verifiableCredential)
    expect(didResolver.resolve).toHaveBeenCalledWith('https://example.com/issuer/123')
  })

  it('should raise an exception if proof type is not JsonWebSignature2020', async () => {
    const incorrectVerifiableCredential: any = structuredClone(verifiableCredential)
    incorrectVerifiableCredential.proof.type = 'WrongSignatureType2020'

    try {
      await verifier.verify(incorrectVerifiableCredential)
    } catch (e) {
      expect(e).toBeInstanceOf(MalformedProofException)
      const error: MalformedProofException = e as MalformedProofException

      expect(error.message).toEqual('WrongSignatureType2020 proof type is not supported')
      expect(didResolver.resolve).not.toHaveBeenCalled()

      return
    }

    throw new Error()
  })

  it.each(['bad format', ' '])(
    'should raise an exception if proof created date property is provided and incorrect',
    async (created: string | null | undefined) => {
      const incorrectVerifiableCredential: any = structuredClone(verifiableCredential)
      incorrectVerifiableCredential.proof.created = created

      try {
        await verifier.verify(incorrectVerifiableCredential)
      } catch (e) {
        expect(e).toBeInstanceOf(MalformedProofException)
        const error: MalformedProofException = e as MalformedProofException

        expect(error.message).toEqual(`Please provide a valid proof 'created' attribute, ${created} is invalid`)
        expect(didResolver.resolve).not.toHaveBeenCalled()

        return
      }

      throw new Error()
    }
  )

  it.each([null, undefined])('should validate proof with missing created date', async (created: null | undefined) => {
    const incorrectVerifiableCredential: any = structuredClone(verifiableCredential)
    incorrectVerifiableCredential.proof.created = created

    try {
      await verifier.verify(incorrectVerifiableCredential)
    } catch (e) {
      expect(e).toBeInstanceOf(SignatureValidationException)
      expect(didResolver.resolve).toHaveBeenCalled()

      return
    }

    throw new Error()
  })

  it.each([null, undefined, ''])('should raise an exception if proof JWS is missing', async (jws: string | null | undefined) => {
    const incorrectVerifiableCredential: any = structuredClone(verifiableCredential)
    incorrectVerifiableCredential.proof.jws = jws

    try {
      await verifier.verify(incorrectVerifiableCredential)
    } catch (e) {
      expect(e).toBeInstanceOf(MalformedProofException)
      const error: MalformedProofException = e as MalformedProofException

      expect(error.message).toEqual(`The 'jws' attribute is missing in the proof, this isn't a valid verifiable credential`)
      expect(didResolver.resolve).not.toHaveBeenCalled()

      return
    }

    throw new Error()
  })

  it('should raise an exception when verification method cannot be found', async () => {
    const incorrectVerificationMethod: VerifiableCredential = structuredClone(verifiableCredential)
    incorrectVerificationMethod.proof.verificationMethod = 'https://example.com/issuer/123#missing'

    try {
      await verifier.verify(incorrectVerificationMethod)
    } catch (e) {
      expect(e).toBeInstanceOf(UnresolvableVerificationMethodException)
      const error: UnresolvableVerificationMethodException = e as UnresolvableVerificationMethodException

      expect(error.message).toEqual(
        `The ${incorrectVerificationMethod.proof.verificationMethod} verification method cannot be resolved, please check it's exposed in the corresponding DID document`
      )
      expect(didResolver.resolve).toHaveBeenCalledWith('https://example.com/issuer/123')

      return
    }

    throw new Error()
  })

  it('should raise an exception when verification method and public key cannot be found', async () => {
    vi.spyOn(didResolver, 'resolve').mockImplementation((did: string): Promise<DIDDocument> => {
      return Promise.resolve({
        '@context': ['https://www.w3.org/ns/did/v1', 'https://w3id.org/security/suites/jws-2020/v1'],
        id: 'https://example.com/issuer/123'
      })
    })

    try {
      await verifier.verify(verifiableCredential)
    } catch (e) {
      expect(e).toBeInstanceOf(UnresolvableVerificationMethodException)
      const error: UnresolvableVerificationMethodException = e as UnresolvableVerificationMethodException

      expect(error.message).toEqual(
        `The ${verifiableCredential.proof.verificationMethod} verification method cannot be resolved, please check it's exposed in the corresponding DID document`
      )
      expect(didResolver.resolve).toHaveBeenCalledWith('https://example.com/issuer/123')

      return
    }

    throw new Error()
  })

  it('should raise an exception when document has been tampered', async () => {
    const tamperedDocument: VerifiableCredential = structuredClone(verifiableCredential)
    tamperedDocument.issuer.id = 'http://tampered.com/malicious/issuer'

    try {
      await verifier.verify(tamperedDocument)
    } catch (e) {
      expect(e).toBeInstanceOf(SignatureValidationException)
      const error: SignatureValidationException = e as SignatureValidationException

      expect(error.message).toEqual(
        `The signature of the document with ID ${tamperedDocument.id} cannot be validated, please check the document has not been tampered`
      )
      expect(didResolver.resolve).toHaveBeenCalledWith('https://example.com/issuer/123')

      return
    }

    throw new Error()
  })

  it('should raise an exception when proof has been tampered', async () => {
    const tamperedProof: VerifiableCredential = structuredClone(verifiableCredential)
    tamperedProof.proof.verificationMethod = 'http://tampered.com/malicious/issuer#ovsDKYBjFemIy8DVhc-w2LSi8CvXMw2AYDzHj04yxkc'

    try {
      await verifier.verify(tamperedProof)
    } catch (e) {
      expect(e).toBeInstanceOf(SignatureValidationException)
      const error: SignatureValidationException = e as SignatureValidationException

      expect(error.message).toEqual(
        `The signature of the document with ID ${tamperedProof.id} cannot be validated, please check the document has not been tampered`
      )
      expect(didResolver.resolve).toHaveBeenCalledWith('http://tampered.com/malicious/issuer')

      return
    }

    throw new Error()
  })
})
