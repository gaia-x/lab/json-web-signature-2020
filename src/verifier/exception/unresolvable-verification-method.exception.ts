export class UnresolvableVerificationMethodException extends Error {
  constructor(verificationMethod: string) {
    super(`The ${verificationMethod} verification method cannot be resolved, please check it's exposed in the corresponding DID document`)
  }
}
