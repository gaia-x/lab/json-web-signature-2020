export class MalformedProofException extends Error {
  constructor(message: string) {
    super(message)
  }
}
