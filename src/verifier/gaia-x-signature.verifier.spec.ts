import { DIDDocument } from 'did-resolver'
import { MockedFunction, beforeEach, describe, expect, it, vi } from 'vitest'

import { VerifiableCredential } from '../model/verifiable-credential.model'
import { DidResolver } from '../resolver/did.resolver'
import { GaiaXSignatureVerifier } from './gaia-x-signature.verifier'

vi.mock('../resolver/did.resolver')

describe('GaiaXSignatureVerifier', () => {
  const verifiableCredential: VerifiableCredential = {
    '@context': ['https://www.w3.org/2018/credentials/v1', 'https://w3id.org/security/suites/jws-2020/v1'],
    type: ['VerifiableCredential'],
    id: 'did:web:registrationnumber.notary.lab.gaia-x.eu:main:d6e81427-0873-466f-bf85-6334a0984daf',
    issuer: 'did:web:registrationnumber.notary.lab.gaia-x.eu:main',
    issuanceDate: '2024-01-31T14:42:28.182+00:00',
    credentialSubject: {
      '@context': 'https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#',
      id: 'did:web:gaia-x.eu:legalRegistrationNumber.json',
      type: 'gx:legalRegistrationNumber',
      'gx:vatID': 'FR79537407926',
      'gx:vatID-countryCode': 'FR'
    },
    evidence: [
      {
        'gx:evidenceOf': 'VAT_ID',
        'gx:evidenceURL': 'http://ec.europa.eu/taxation_customs/vies/services/checkVatService',
        'gx:executionDate': '2024-01-31T14:42:28.178+00:00'
      }
    ],
    proof: {
      type: 'JsonWebSignature2020',
      created: '2024-01-31T14:42:29.220+00:00',
      proofPurpose: 'assertionMethod',
      verificationMethod: 'did:web:registrationnumber.notary.lab.gaia-x.eu:main#X509-JWK2020',
      jws: 'eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..LUWioY2T1gbOOA-YRDMGFRZn7xgzRxZXCcppxzWvvVSge0JSjFo5gQvbIGnHvVYlgerrl4yvV82afGRwrpC4yfLPkhBApPXVJEmGq5BGE6P1Gmu0v3GWU1Ajl9Byh8iw9GzRqW-ku_sFaBMZytQyqQ_-5XUSmNgVzXOvbbaWvga598RKqbTyyLh0ig0M72anDdf7XfqBD7ljQvuIMh-ZLz4YohyfKatl74rFIaMrRtKK2KIGoPrAfjbGVpuWJAdC_UioEoSzD_jmRwx6GLXzAxDBTGfvpyaUWm5GnBJPf58EduB5HrEhgsYSenlXJXQp-MkAI6Sk40YnuwgwGmrVFA'
    }
  }

  const didResolver: DidResolver = new DidResolver()
  const verifier: GaiaXSignatureVerifier = new GaiaXSignatureVerifier({ didResolver })

  beforeEach(() => {
    ;(didResolver.resolve as MockedFunction<typeof didResolver.resolve>).mockClear()
    vi.spyOn(didResolver, 'resolve').mockImplementation((did: string): Promise<DIDDocument> => {
      return Promise.resolve({
        '@context': ['https://www.w3.org/ns/did/v1', 'https://w3id.org/security/suites/jws-2020/v1'],
        id: 'did:web:registrationnumber.notary.lab.gaia-x.eu:main',
        verificationMethod: [
          {
            '@context': 'https://w3c-ccg.github.io/lds-jws2020/contexts/v1/',
            id: 'did:web:registrationnumber.notary.lab.gaia-x.eu:main#X509-JWK2020',
            type: 'JsonWebKey2020',
            controller: 'did:web:registrationnumber.notary.lab.gaia-x.eu:main',
            publicKeyJwk: {
              kty: 'RSA',
              n: 'ulmXEa0nehbR338h6QaWLjMqfXE7mKA9PXoC_6_8d26xKQuBKAXa5k0uHhzQfNlAlxO-IpCDgf9cVzxIP-tkkefsjrXc8uvkdKNK6TY9kUxgUnOviiOLpHe88FB5dMTH6KUUGkjiPfq3P0F9fXHDEoQkGSpWui7eD897qSEdXFre_086ns3I8hSVCxoxlW9guXa_sRISIawCKT4UA3ZUKYyjtu0xRy7mRxNFh2wH0iSTQfqf4DWUUThX3S-jeRCRxqOGQdQlZoHym2pynJ1IYiiIOMO9L2IQrQl35kx94LGHiF8r8CRpLrgYXTVd9U17-nglrUmJmryECxW-555ppQ',
              e: 'AQAB',
              alg: 'PS256',
              x5u: 'https://registrationnumber.notary.lab.gaia-x.eu/main/.well-known/x509CertificateChain.pem'
            }
          }
        ],
        assertionMethod: ['did:web:registrationnumber.notary.lab.gaia-x.eu:main#X509-JWK2020']
      })
    })
  })

  it('should verify a valid Gaia-X verifiable credential', async () => {
    await verifier.verify(verifiableCredential)
    expect(didResolver.resolve).toHaveBeenCalledWith('did:web:registrationnumber.notary.lab.gaia-x.eu:main')
  })
})
