import { GaiaXSignatureHasher } from '../hasher/gaia-x-signature.hasher'
import { VerifierOptions } from '../options/verifier.options'
import { Verifier } from './verifier'

/**
 *  Allows to verify verifiable credential proofs that were previously issued by Gaia-X Clearing Houses before version
 *  Loire.
 *  <p>
 *  The difference with the JsonWebSignature2020 specification is that the proof isn't canonized/normalized,
 *  hashed and integrated in the final detached JWS signature.
 *  </p>
 */
export class GaiaXSignatureVerifier extends Verifier {
  constructor(options?: VerifierOptions) {
    super(new GaiaXSignatureHasher(options), options)
  }
}
