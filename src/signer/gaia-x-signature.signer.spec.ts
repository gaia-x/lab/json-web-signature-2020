import { DIDDocument, JsonWebKey } from 'did-resolver'
import { exportJWK, generateKeyPair } from 'jose'
import { describe, expect, it, vi } from 'vitest'

import { DocumentLoader } from '../jsonld/jsonld.types'
import { OfflineDocumentLoaderBuilder } from '../jsonld/offline-document.loader'
import { JWS2020_CONTEXT } from '../model/proof.model'
import { VerifiableCredential } from '../model/verifiable-credential.model'
import { DidResolver } from '../resolver/did.resolver'
import { GaiaXSignatureVerifier } from '../verifier/gaia-x-signature.verifier'
import { GaiaXSignatureSigner } from './gaia-x-signature.signer'

vi.mock('../resolver/did.resolver')

describe('GaiaXSignatureSigner', () => {
  const document: Omit<VerifiableCredential, 'proof'> = {
    '@context': ['https://www.w3.org/2018/credentials/v1', 'https://www.w3.org/2018/credentials/examples/v1'],
    id: 'http://example.edu/credentials/1234',
    type: ['VerifiableCredential', 'AlumniCredential'],
    issuer: 'https://example.edu/issuers/123456',
    issuanceDate: '2024-02-01T15:34:02Z',
    credentialSubject: {
      id: 'did:example:456a9956-fc4b-46c7-bb6e-ba27a9ad0261',
      alumniOf: {
        id: 'did:example:bb578304-9a4f-46dd-9caf-e9b806594b25'
      },
      name: 'Jane Doe'
    }
  }

  const offlineDocumentLoader: DocumentLoader = new OfflineDocumentLoaderBuilder().build()

  it.concurrent.each(['EdDSA', 'RS256', 'RS384', 'RS512', 'PS256', 'PS384', 'PS512', 'ES256', 'ES256K', 'ES384', 'ES512'])(
    'should sign a verifiable credential v1 document with a %s private key',
    async privateKeyAlg => {
      const didResolver: DidResolver = new DidResolver()
      const { publicKey, privateKey } = await generateKeyPair(privateKeyAlg)

      vi.spyOn(didResolver, 'resolve').mockImplementation(async (did: string): Promise<DIDDocument> => {
        return Promise.resolve({
          '@context': ['https://www.w3.org/ns/did/v1', 'https://w3id.org/security/suites/jws-2020/v1'],
          id: 'did:web:example.edu',
          verificationMethod: [
            {
              '@context': 'https://w3c-ccg.github.io/lds-jws2020/contexts/v1/',
              id: 'did:web:example.edu#key-1',
              type: 'JsonWebKey2020',
              controller: 'did:web:example.edu',
              publicKeyJwk: (await exportJWK(publicKey)) as JsonWebKey
            }
          ],
          assertionMethod: ['did:web:example.edu#key-1']
        })
      })

      const signer: GaiaXSignatureSigner = new GaiaXSignatureSigner({
        privateKey,
        privateKeyAlg,
        verificationMethod: 'did:web:example.edu#key-1',
        documentLoader: offlineDocumentLoader,
        safe: true
      })
      const verifiableCredential: VerifiableCredential = await signer.sign(document)
      expect(verifiableCredential.proof['@context']).toBeUndefined()
      expect(verifiableCredential['@context']).toContain(JWS2020_CONTEXT)

      const verifier: GaiaXSignatureVerifier = new GaiaXSignatureVerifier({ didResolver, documentLoader: offlineDocumentLoader, safe: true })
      await verifier.verify(verifiableCredential)
      expect(didResolver.resolve).toHaveBeenCalledWith('did:web:example.edu')
    }
  )
})
