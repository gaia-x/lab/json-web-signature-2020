import { GaiaXSignatureHasher } from '../hasher/gaia-x-signature.hasher'
import { SignerOptions } from '../options/signer.options'
import { Signer } from './signer'

/**
 * Used to sign verifiable credentials with the original Gaia-X signing method.
 */
export class GaiaXSignatureSigner extends Signer {
  constructor(options: SignerOptions) {
    super(new GaiaXSignatureHasher(options), options)
  }
}
