import { CompactSign, KeyLike } from 'jose'
import { DateTime } from 'luxon'

import { Hasher } from '../hasher/hasher'
import { ContextHandler } from '../jsonld/context.handler'
import { JWS2020_CONTEXT } from '../model/proof.model'
import { VerifiableCredential } from '../model/verifiable-credential.model'
import { SignerOptions } from '../options/signer.options'

/**
 * In charge of transforming JSON-LD documents into verifiable credentials, the Signer is the master of proof creation.
 */
export abstract class Signer {
  private readonly hasher: Hasher
  private readonly privateKey: KeyLike | Uint8Array
  private readonly privateKeyAlg: string
  private readonly verificationMethod: string

  protected constructor(hasher: Hasher, options: SignerOptions) {
    this.hasher = hasher
    this.privateKey = options.privateKey!
    this.privateKeyAlg = options.privateKeyAlg!
    this.verificationMethod = options.verificationMethod!
  }

  /**
   * Signs a document to make it an untamperable verifiable credential.
   *
   * @param document the document to sign
   */
  async sign(document: Omit<VerifiableCredential, 'proof'>): Promise<VerifiableCredential> {
    const contextHandler: ContextHandler = ContextHandler.fromContext(document['@context']).add(JWS2020_CONTEXT)

    const verifiableCredential: VerifiableCredential = {
      ...document,
      '@context': contextHandler.toContext(),
      proof: {
        type: 'JsonWebSignature2020',
        created: DateTime.now().toISO(),
        proofPurpose: 'assertionMethod',
        verificationMethod: this.verificationMethod
      }
    }

    const hash: Buffer | Uint8Array = await this.hasher.hash(document, verifiableCredential.proof)

    verifiableCredential.proof.jws = await new CompactSign(hash)
      .setProtectedHeader({
        alg: this.privateKeyAlg,
        b64: false,
        crit: ['b64']
      })
      .sign(this.privateKey)

    return verifiableCredential
  }
}
