import { defineConfig } from 'vitest/config'

export default defineConfig({
  test: {
    coverage: {
      provider: 'v8',
      reporter: ['text', 'lcov'],
      exclude: ['commitlint.config.js', 'src/jsonld/jsonld.types.ts', 'src/model/*.model.ts', 'index.ts', 'src/options/*']
    }
  }
})
